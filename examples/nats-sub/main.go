// Copyright 2012-2019 The NATS Authors
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"runtime"
	"time"

	"github.com/nats-io/nats.go"
	"github.com/ohler55/ojg/oj"
)

// NOTE: Can test with demo servers.
// nats-sub -s demo.nats.io <subject>
// nats-sub -s demo.nats.io:4443 <subject> (TLS version)

func usage() {
	log.Printf("Usage: nats-sub [-s server] [-creds file] [-t] <subject>\n")
	flag.PrintDefaults()
}

func showUsageAndExit(exitcode int) {
	usage()
	os.Exit(exitcode)
}

func printMsg(m *nats.Msg, i int, parser ...*oj.Parser) {
	if len(parser) < 1 {
		fmt.Fprintf(os.Stderr, "[#%d] Received on [%s]:\n", i, m.Subject)
		fmt.Printf("%s\n", m.Data)
		return
	}
	obj := map[string]interface{}{
		"_meta": map[string]interface{}{
			"i":       i,
			"subject": m.Subject,
		},
	}
	msg, err := parser[0].Parse(m.Data)
	if err == nil {
		obj["data"] = msg
	} else {
		obj["data"] = string(m.Data)
		obj["_meta"].(map[string]interface{})["error"] = err.Error()
	}
	fmt.Println(oj.JSON(obj))
}

func main() {
	var urls = flag.String("s", nats.DefaultURL, "The nats server URLs (separated by comma)")
	var tlsCA = flag.String("tlsca", "", "Use TLS Secure Connection. CA")
	var tlsKey = flag.String("tlskey", "", "Use TLS Secure Connection. Key")
	var tlsCert = flag.String("tlscert", "", "Use TLS Secure Connection. Cert")
	var userCreds = flag.String("creds", "", "User Credentials File")
	var showTime = flag.Bool("t", false, "Display timestamps")
	var showHelp = flag.Bool("h", false, "Show help message")
	var jsonMode = flag.Bool("json", false, "Parse responses as json and embed in a message object")

	log.SetFlags(0)
	flag.Usage = usage
	flag.Parse()

	if *showHelp {
		showUsageAndExit(0)
	}

	args := flag.Args()
	if len(args) != 1 {
		showUsageAndExit(1)
	}

	// Connect Options.
	opts := []nats.Option{nats.Name("NATS Sample Subscriber")}
	opts = setupConnOptions(opts)

	// Use Client Certificates
	if *tlsCA != "" {
		opts = append(opts, nats.RootCAs(*tlsCA))
	}
	if *tlsCert != "" && *tlsKey != "" {
		opts = append(opts, nats.ClientCert(*tlsCert, *tlsKey))
	}

	// Use UserCredentials
	if *userCreds != "" {
		opts = append(opts, nats.UserCredentials(*userCreds))
	}

	// Connect to NATS
	nc, err := nats.Connect(*urls, opts...)
	if err != nil {
		log.Fatal(err)
	}

	subj, i := args[0], 0

	var parser oj.Parser
	nc.Subscribe(subj, func(msg *nats.Msg) {
		i += 1
		if *jsonMode {
			printMsg(msg, i, &parser)
		} else {
			printMsg(msg, i)
		}
	})
	nc.Flush()

	if err := nc.LastError(); err != nil {
		log.Fatal(err)
	}

	log.Printf("Listening on [%s]", subj)
	if *showTime {
		log.SetFlags(log.LstdFlags)
	}

	runtime.Goexit()
}

func setupConnOptions(opts []nats.Option) []nats.Option {
	totalWait := 10 * time.Minute
	reconnectDelay := time.Second

	opts = append(opts, nats.ReconnectWait(reconnectDelay))
	opts = append(opts, nats.MaxReconnects(int(totalWait/reconnectDelay)))
	opts = append(opts, nats.DisconnectErrHandler(func(nc *nats.Conn, err error) {
		log.Printf("Disconnected due to:%s, will attempt reconnects for %.0fm", err, totalWait.Minutes())
	}))
	opts = append(opts, nats.ReconnectHandler(func(nc *nats.Conn) {
		log.Printf("Reconnected [%s]", nc.ConnectedUrl())
	}))
	opts = append(opts, nats.ClosedHandler(func(nc *nats.Conn) {
		log.Fatalf("Exiting: %v", nc.LastError())
	}))
	return opts
}
