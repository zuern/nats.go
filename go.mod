module github.com/nats-io/nats.go

require (
	github.com/golang/protobuf v1.3.5
	github.com/nats-io/jwt v0.3.2
	github.com/nats-io/nats-server/v2 v2.1.4
	github.com/nats-io/nkeys v0.1.3
	github.com/nats-io/nuid v1.0.1
	github.com/ohler55/ojg v1.1.4
)

go 1.13
